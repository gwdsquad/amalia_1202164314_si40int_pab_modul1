package id.amaliafiqhiyah.amalia_1202164314_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public EditText edAlas, edTinggi;
    public Button btnCek;
    public TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edAlas = (EditText)findViewById(R.id.alas);
        edTinggi = (EditText)findViewById(R.id.tinggi);
        btnCek = (Button)findViewById(R.id.btn_cek);
        hasil = (TextView)findViewById(R.id.hasil);

        btnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                perhitungan(v);
            }
        });
    }

    public void perhitungan(View view) {
        int alas = Integer.parseInt(edAlas.getText().toString());
        int tinggi = Integer.parseInt(edTinggi.getText().toString());
        int cekHasil = alas * tinggi;
        hasil.setText("Luasnya adalah " +cekHasil);
    }
}
